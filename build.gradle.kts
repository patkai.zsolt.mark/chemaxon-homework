plugins {
    java
}



group = "io.patkaimark"
version = "1.0-SNAPSHOT"


repositories {
    mavenCentral()
}


dependencies {

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")
    annotationProcessor("org.projectlombok:lombok:1.18.20")
    compileOnly("org.projectlombok:lombok:1.18.20")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.20")
    testImplementation("org.projectlombok:lombok:1.18.20")
    implementation("org.jgrapht:jgrapht-core:1.5.1")
    implementation("org.slf4j:slf4j-simple:1.7.32")
    testImplementation("org.assertj:assertj-core:3.20.2")
    testImplementation("org.mockito:mockito-core:3.12.4")

}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<AbstractArchiveTask> {
    setProperty("archiveFileName", "chemaxon.jar")
}

tasks.withType<JavaCompile> {
    sourceCompatibility = "11"
    targetCompatibility = "11"
}