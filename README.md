##Chemaxon Homework

Application reads filenames from console.

**NOTE**: The application always prompts for an input.
Only way to exit is giving the application the
`exit` command!

**NOTE**: Requires Java 11!

##SimpleLogger/SLF4j setup

Defualt Logger Level --> INFO


##Basic Usage

1. Compile with gradlew
```
gradlew clean build
```
2. Browse to build/libs and find `chemaxon.jar` and run
```
java -jar chemaxon.jar
```
3. Default console file prompt looks like this:
```
Enter a file name or 'exit' to quit program: 
```
4. Enter your file name and location (no location means current folder).
5. Wait until the program finishes mapping the current file.
6. You can read the output in the console itself (no output implemented).

## Example files

`SHORT_CASE_VALID.txt` as input:
```
[main] INFO io.patkaimark.chemaxon.Application - #######################################################################################
[main] INFO io.patkaimark.chemaxon.Application - Added GRAPH[0]: ([A, B, C], [{A,B}, {B,C}])
[main] INFO io.patkaimark.chemaxon.Application - Graph's spanning tree is: Spanning-Tree [weight=2.0, edges=[(A : B), (B : C)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number edges spanning tree: 2
[main] INFO io.patkaimark.chemaxon.Application - Graph's cycles are: []
[main] INFO io.patkaimark.chemaxon.Application - Graph's number of cycles are: 0
[main] INFO io.patkaimark.chemaxon.Application - Graph's short notation from graph is: ABC
[main] INFO io.patkaimark.chemaxon.Application - #######################################################################################
[main] INFO io.patkaimark.chemaxon.Application - Added GRAPH[1]: ([A, B, C], [{A,B}, {B,C}])
[main] INFO io.patkaimark.chemaxon.Application - Graph's spanning tree is: Spanning-Tree [weight=2.0, edges=[(A : B), (B : C)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number edges spanning tree: 2
[main] INFO io.patkaimark.chemaxon.Application - Graph's cycles are: []
[main] INFO io.patkaimark.chemaxon.Application - Graph's number of cycles are: 0
[main] INFO io.patkaimark.chemaxon.Application - Graph's short notation from graph is: ABC
[main] INFO io.patkaimark.chemaxon.Application - #######################################################################################
[main] INFO io.patkaimark.chemaxon.Application - Added GRAPH[2]: ([A, B, C], [{B,C}])
[main] INFO io.patkaimark.chemaxon.Application - Graph's spanning tree is: Spanning-Tree [weight=1.0, edges=[(B : C)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number edges spanning tree: 1
[main] INFO io.patkaimark.chemaxon.Application - Graph's cycles are: []
[main] INFO io.patkaimark.chemaxon.Application - Graph's number of cycles are: 0
[main] INFO io.patkaimark.chemaxon.Application - Graph's short notation from graph is: BC.A
[main] INFO io.patkaimark.chemaxon.Application - #######################################################################################
[main] INFO io.patkaimark.chemaxon.Application - Added GRAPH[3]: ([A, B, C], [{A,B}, {B,C}, {C,A}])
[main] INFO io.patkaimark.chemaxon.Application - Graph's spanning tree is: Spanning-Tree [weight=2.0, edges=[(A : B), (B : C)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number edges spanning tree: 2
[main] INFO io.patkaimark.chemaxon.Application - Graph's cycles are: [[(A : B), (B : C), (C : A)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number of cycles are: 1
[main] INFO io.patkaimark.chemaxon.Application - Graph's short notation from graph is: ABCA
[main] INFO io.patkaimark.chemaxon.Application - #######################################################################################
[main] INFO io.patkaimark.chemaxon.Application - Added GRAPH[4]: ([A, B, C], [{B,C}])
[main] INFO io.patkaimark.chemaxon.Application - Graph's spanning tree is: Spanning-Tree [weight=1.0, edges=[(B : C)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number edges spanning tree: 1
[main] INFO io.patkaimark.chemaxon.Application - Graph's cycles are: []
[main] INFO io.patkaimark.chemaxon.Application - Graph's number of cycles are: 0
[main] INFO io.patkaimark.chemaxon.Application - Graph's short notation from graph is: BC.A
[main] INFO io.patkaimark.chemaxon.Application - #######################################################################################
[main] INFO io.patkaimark.chemaxon.Application - Added GRAPH[5]: ([A, B, C, D, E], [{A,B}, {B,C}, {C,D}, {C,E}, {A,D}, {E,D}])
[main] INFO io.patkaimark.chemaxon.Application - Graph's spanning tree is: Spanning-Tree [weight=4.0, edges=[(A : B), (B : C), (C : D), (C : E)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number edges spanning tree: 4
[main] INFO io.patkaimark.chemaxon.Application - Graph's cycles are: [[(A : B), (B : C), (C : D), (A : D)], [(C : D), (C : E), (E : D)]]
[main] INFO io.patkaimark.chemaxon.Application - Graph's number of cycles are: 2
[main] INFO io.patkaimark.chemaxon.Application - Graph's short notation from graph is: ABCDCEADED
```

`SHORT_CASE_INVALID.txt` result is:

```
[main] ERROR io.patkaimark.chemaxon.Application - Exception occurred: Invalid line at 9
io.patkaimark.chemaxon.exceptions.ChemException: Invalid line at 9
	at io.patkaimark.chemaxon.engines.ReadEngine.lineReader(ReadEngine.java:123)
	at io.patkaimark.chemaxon.Application.run(Application.java:55)
	at io.patkaimark.chemaxon.Application.main(Application.java:32)
```

Line at 9 is: 
```
A.BC2
```

## Common Exceptions:

- `io.patkaimark.chemaxon.exceptions.ChemException: Invalid line at X:`
Something doesn't match at that line with the syntax and semantics
- `io.patkaimark.chemaxon.exceptions.ChemException: While reading from console, an error occurred:`
There is problem with the input file given to the program.

