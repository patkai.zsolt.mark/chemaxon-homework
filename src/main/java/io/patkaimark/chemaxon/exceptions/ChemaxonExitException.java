package io.patkaimark.chemaxon.exceptions;

public class ChemaxonExitException extends Exception {

    public ChemaxonExitException() {
    }

    public ChemaxonExitException(String message) {
        super(message);
    }
}
