package io.patkaimark.chemaxon.exceptions;

public class ChemException extends Exception {

    public ChemException(String message) {
        super(message);
    }

    public ChemException(String message, Throwable cause) {
        super(message, cause);
    }
}
