package io.patkaimark.chemaxon;

import io.patkaimark.chemaxon.engines.GraphEngine;
import io.patkaimark.chemaxon.engines.ReadEngine;
import io.patkaimark.chemaxon.exceptions.ChemException;
import io.patkaimark.chemaxon.exceptions.ChemaxonExitException;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class Application {

    private static ReadEngine readEngine;
    private static GraphEngine graphEngine;


    /**
     * Application main method with a global exception handler
     */
    public static void main(String[] args) {
        try {
            log.debug("Application has started!");
            run();
        }catch (ChemException cex) {
            log.error("Exception occurred: {}", cex.getMessage(), cex);
        }catch (ChemaxonExitException exit) {
            return;
        }catch (Exception ex) {
            log.error("Unhandled exception occurred: ",ex);
        }
        main(args);
    }

    /**
     * Main program logic
     */
    private static void run() throws ChemException, ChemaxonExitException {
        try {
            initApplication();

            System.out.print("Enter a file name or 'exit' to quit program: ");
            String readLine = reader().readLine();
            System.out.print("\n");
            while(!readLine.equals("exit")){
                InputStream streamFromFile = readEngine.getStreamFromFile(readLine);
                List<String> lines = readEngine.lineReader(streamFromFile);
                AtomicInteger i = new AtomicInteger();
                for (String y : lines) {
                    Graph<String, DefaultEdge> graph = graphEngine.createGraph(i.get(), y);
                    log.info("#######################################################################################");
                    log.info("Added GRAPH[{}]: {}", i, graph);
                    log.info("Graph's spanning tree is: {}", graphEngine.getSpanningTree(i.get()));
                    log.info("Graph's number edges spanning tree: {}", graphEngine.numberOfEdgesSpanningTree(i.get()));
                    log.info("Graph's cycles are: {}", graphEngine.getFundamentalCycle(i.get()));
                    log.info("Graph's number of cycles are: {}", graphEngine.numberOfCycles(i.get()));
                    
                    log.info("Graph's short notation from graph is: {}", graphEngine.getShortNotation(i.get()));
                    i.getAndIncrement();
                }
                System.out.print("Enter a file name or 'exit' to quit program: ");
                readLine = reader().readLine();
            }
            throw new ChemaxonExitException();
        } catch (IOException e) {
            throw new ChemException("While reading from console, an error occurred", e);
        }
    }

    /**
     * Initialize Stateful variables
     * @throws ChemException See {@link ReadEngine}
     */
    private static void initApplication() throws ChemException {
        readEngine = ReadEngine.getInstance();
        graphEngine = GraphEngine.getInstance();
    }


    /**
     * Shorthand parameter for console inputs
     * @return Reader for console inputs from {@link System}
     */
    private static BufferedReader reader() {
        return new BufferedReader(new InputStreamReader(System.in));
    }
}
