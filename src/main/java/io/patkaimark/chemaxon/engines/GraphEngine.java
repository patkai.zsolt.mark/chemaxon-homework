package io.patkaimark.chemaxon.engines;

import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.alg.cycle.StackBFSFundamentalCycleBasis;
import org.jgrapht.alg.interfaces.SpanningTreeAlgorithm;
import org.jgrapht.alg.spanning.BoruvkaMinimumSpanningTree;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.util.*;

import static java.util.Objects.isNull;


/**
 * A stateful singleton class of Graph related actions
 *
 * @author Patkai Mark
 */
@Slf4j
public class GraphEngine {


    private static GraphEngine instance = null;

    public static GraphEngine getInstance() {
        if(isNull(instance)) instance = new GraphEngine();
        return instance;
    }

    private Map<Integer, Graph<String, DefaultEdge>> graphMap;

    private GraphEngine() {
        graphMap = new HashMap<>();
    }

    /**
     * Finds all unique characters from {@param input}
     * @param input Validated input
     * @return {@link Set} of Strings
     */
    public Set<String> findSetOfVertexes(String input) {
        Set<String> res = new HashSet<>();
        for (Character c : input.toCharArray()) {
            if(!c.equals('.') && !c.equals('-')) res.add(c.toString());
        }
        log.debug("Found set of Vertex for Input[{}], Vertices are: [{}].", input, res);
        return res;
    }

    /**
     * Creates a graph according to the validated input
     * @param mapId Unique map id in stateful
     * @param input Validated input
     * @return Returns a {@link Graph}&lt{@link String},{@link DefaultEdge}&gt graph
     */
    public Graph<String, DefaultEdge> createGraph(int mapId, String input) {
        Set<String> setOfVertexes = findSetOfVertexes(input);
        Set<String> setOfEdges = findEdges(input);

        Graph<String, DefaultEdge> graph = addVertexesToGraph(createDefaultGraph(), setOfVertexes);


        addEdgesToGraph(graph, setOfEdges);

        graphMap.put(mapId, graph);

        return graphMap.get(mapId);
    }

    /**
     * Returns the mapped Graph from the state
     * @param mapId The Graph's id in the map
     * @return Returns the mapped {@param mapId} {@link Graph}&lt{@link String},{@link DefaultEdge}&gt graph
     */
    public Graph<String, DefaultEdge> getGraph(int mapId) {
        return graphMap.get(mapId);
    }

    /**
     * Creates the default, empty, undirected graph using {@link SimpleGraph}
     * @return Returns an empty {@link Graph}&lt{@link String},{@link DefaultEdge}&gt graph
     */
    private Graph<String, DefaultEdge> createDefaultGraph() {
        return new SimpleGraph<>(DefaultEdge.class);
    }

    /**
     * Maps the vertexes to the Graph
     * @param graph Graph to add vertices to
     * @param vertex {@link Set} of vertices
     * @return Returns the Vertexful {@link Graph}&lt{@link String},{@link DefaultEdge}&gt graph
     */
    public Graph<String, DefaultEdge> addVertexesToGraph(Graph<String, DefaultEdge> graph, Set<String> vertex) {
        vertex.forEach(graph::addVertex);
        return graph;
    }

    /**
     * Maps the edges to Vertices
     * @param graph Graph to work on
     * @param edges {@link Set} of edges
     */
    public void addEdgesToGraph(Graph<String, DefaultEdge> graph, Set<String> edges) {
        edges.forEach(y -> {
            String left = String.valueOf(y.charAt(0));
            String right = String.valueOf(y.charAt(1));
            graph.addEdge(left, right);
        });

    }

    /**
     * Finds edges from the validated input
     * @param input Validated input
     * @return {@link Set} of edges
     */
    public Set<String> findEdges(String input) {
        Set<String> res = new HashSet<>();
        for (int i = 0; i < input.length() - 1; i++) {
            if(input.charAt(i) == '-') continue;
            if(input.charAt(i+1) == '.' || input.charAt(i) == '.') continue;
            String temp = String.valueOf(input.charAt(i));
            String next = String.valueOf(input.charAt(i + 1));
            if(input.charAt(i+1) == '-') {
                temp += next;
                temp += String.valueOf(input.charAt(i+2));
            }else {
                if(input.charAt(i+1) != '.') temp+= next;
            }
            res.add(temp.replace("-", ""));
        }
        log.debug("Found set of Edges for Input[{}], Edges are: [{}].", input, res);
        return res;
    }

    /**
     * Gets the minimum spanning tree using <a href=https://en.wikipedia.org/wiki/Bor%C5%AFvka%27s_algorithm>Boruvka's Algorithm</a>
     * @param mapId Graph's stateful map id
     * @return Returns {@link SpanningTreeAlgorithm.SpanningTree}&lt{@link DefaultEdge}&gt
     */
    public SpanningTreeAlgorithm.SpanningTree<DefaultEdge> getSpanningTree(int mapId) {
        return new BoruvkaMinimumSpanningTree<>(graphMap.get(mapId)).getSpanningTree();
    }

    /**
     * See at {@link StackBFSFundamentalCycleBasis}
     * @param mapId Graph's stateful map id
     * @return {@link Set}&lt{@link List}@lt{@link DefaultEdge}&gt&gt
     */
    public Set<List<DefaultEdge>> getFundamentalCycle(int mapId) {
        return new StackBFSFundamentalCycleBasis<>(graphMap.get(mapId)).getCycleBasis().getCycles();
    }

    /**
     * Creates short notation of the graph
     * @param mapId Graph's stateful map id
     * @return Valid short notation
     */
    public String getShortNotation(int mapId) {
        Graph<String, DefaultEdge> graph = graphMap.get(mapId);
        Set<String> vertices = graph.vertexSet();
        Set<DefaultEdge> edges = graph.edgeSet();
        StringBuilder sb = new StringBuilder();
        edges.forEach(y -> {
            String edge = y.toString().replace("(", "").replace(")", "");
            String[] split = edge.split(":");
            if(sb.toString().length() == 0) sb.append(split[0].trim());
            if(sb.toString().length() > 0 &&
                    sb.toString().charAt(sb.toString().length()-1) != split[0].charAt(0))
                sb.append(split[0].trim());

            sb.append(split[1].trim());
        });
        vertices.forEach(y -> {
            String current = sb.toString();
            if(!current.contains(y)) sb.append(String.format("%s%s", ".",y));
        });
        return sb.toString();
    }

    /**
     * Counts the number of edges in the spanning tree
     * @param mapId Graph's stateful map id
     * @return Number of edges
     */
    public int numberOfEdgesSpanningTree(int mapId) {
        return getSpanningTree(mapId).getEdges().size();
    }
    /**
     * Counts the number of cycles in the graph
     * @param mapId Graph's stateful map id
     * @return Number of cycles
     */
    public int numberOfCycles(int mapId) {
        return getFundamentalCycle(mapId).size();
    }
}
