package io.patkaimark.chemaxon.engines;

import io.patkaimark.chemaxon.exceptions.ChemException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static java.util.Objects.isNull;


/**
 *  A stateful singleton Read actions related class
 *
 *
 * @author Patkai Mark
 */
@Slf4j
public class ReadEngine {
    private static final String REGEXP = "^[A-Z\\-.]+$";


    private static ReadEngine INSTANCE;

    /**
     * Retrieves the singleton instance of the ReadEngine
     * @return Instance of ReadEngine
     * @throws ChemException Thrown when RegExp is not valid
     */
    public static ReadEngine getInstance() throws ChemException {
        if(isNull(INSTANCE)) {
            try {
                INSTANCE = new ReadEngine(Pattern.compile(REGEXP));
            }catch (PatternSyntaxException pse) {
                throw new ChemException("Pattern is not valid!", pse);
            }
        }
        return INSTANCE;
    }

    @Getter
    private Matcher matcher;

    private final Pattern pattern;

    private ReadEngine(Pattern p) {
        pattern = p;
    }

    private void createMatcher(String input) {
        matcher = pattern.matcher(input);
    }

    /**
     * Validates the input if it has correct characters according to the RegExp
     * @param input Input string
     * @return True or False value depending on the result of the {@link Matcher}
     */
    public boolean validateInputAsString(String input) {
        createMatcher(input);
        return matcher.matches();
    }

    /**
     *  Checks symbol validity according to these steps:
     *  1. Whether the {@param input} ends with a symbol
     *  2. Two adjacent characters are not symbols
     * @param input Input String
     * @return True or False according to calculation
     */
    public boolean validateInputAsSymbols(String input) {
        if( input.charAt(input.length() - 1) == '.' || input.charAt(input.length() - 1) == '-') return false;
        for (int i = 0; i < input.length() -1; i++) {
            if(input.charAt(i) == '-' || input.charAt(i) == '.')
                return !(input.charAt(i+1) == '-' || input.charAt(i+1) == '.');
        }
        return true;
    }

    /**
     * A summarized version of both validation method
     * This is the preferred method to validate {@param input}
     * @param input Input string
     * @return Result of the validation process : True or False
     */
    public boolean validateInput(String input) {
        return validateInputAsString(input) && validateInputAsSymbols(input);
    }

    /**
     * Returns an {@link InputStream} from path/file name
     * @param file File location and name
     * @return InputStream to read from
     * @throws FileNotFoundException Thrown if file is not found/exists
     */
    public InputStream getStreamFromFile(String file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    /**
     * Reads an input stream, validates it line by line and collects them into an array
     * @param stream {@link InputStream} File's input stream from {@link #getStreamFromFile(String)}
     * @return All validated rows in {@link ArrayList}
     * @throws IOException Thrown when the file unavailable
     * @throws ChemException Thrown when the file has invalid lines
     */
    public List<String> lineReader(InputStream stream) throws IOException, ChemException {
        InputStreamReader reader = new InputStreamReader( stream );
        List<String> result = new ArrayList<>();
        BufferedReader br = new BufferedReader(reader);
        int currentLine = 1;
        while(br.ready()) {
            String line = br.readLine();
            if(validateInput(line)){
                log.debug("Valid Line[{}] found.", line);
                result.add(line);
            }else{
                throw new ChemException("Invalid line at " + currentLine );
            }
            currentLine++;
        }
        br.close();
        reader.close();
        return result;
    }



}
