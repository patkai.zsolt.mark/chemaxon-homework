package io.patkaimark.chemaxontest;

import io.patkaimark.chemaxon.engines.GraphEngine;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class GraphEngineTest {

    public static GraphEngine graphEngine;

    @BeforeAll
    public static void init() {
        graphEngine = GraphEngine.getInstance();
    }

    @Test
    public void findVertexesSimple() {
        log.info("Running findVertexesSimple test for the input string of: AABBCC");
        final String input = "AABBCC";
        Set<String> setOfVertexes = graphEngine.findSetOfVertexes(input);
        assertThat(setOfVertexes).hasSize(3);
        log.info("Set Of Vertex: {}", setOfVertexes.toString());
    }

    @Test
    public void findVertexesDashesAndDots() {
        log.info("Running findVertexesDashesAndDots test for the input string of: AA-BB..CC");
        final String input = "AA-BB..CC";
        Set<String> setOfVertexes = graphEngine.findSetOfVertexes(input);
        assertThat(setOfVertexes).hasSize(3);
        log.info("Set Of Vertex: {}", setOfVertexes.toString());
    }

    @Test
    public void findEdgesSimple() {
        log.info("Running findEdgesSimple test for the input string of: ABC");
        final String input = "ABC";

        Set<String> edges = graphEngine.findEdges(input);

        assertThat(edges).hasSize(2);

        log.info("Set of Edges: {}", edges.toString());
    }

    @Test
    public void findEdgesDashes() {
        log.info("Running findEdgesDashes test for the input string of: A-B-C");
        final String input = "A-B-C";
        Set<String> edges = graphEngine.findEdges(input);
        assertThat(edges).hasSize(2);
        log.info("Set of Edges: {}", edges.toString());
    }

    @Test
    public void findEdgesComplex() {
        log.info("Running findEdgesComplex test for the input string of: A-B.C");
        final String input = "A-B.C";
        Set<String> edges = graphEngine.findEdges(input);
        assertThat(edges).hasSize(1);
        log.info("Set of Edges: {}", edges.toString());
    }

    @Test
    public void testGraphSimple() {
        log.info("Running findEdgesComplex test for the input string of: ABC");
        final String input = "ABC";
        graphEngine.createGraph(1, input);
        Graph<String, DefaultEdge> graph = graphEngine.getGraph(1);
        assertThat(graph).isNotNull();
        log.info("Graph: {}", graph);
    }
}
