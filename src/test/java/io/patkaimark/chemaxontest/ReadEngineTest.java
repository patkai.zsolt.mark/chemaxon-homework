package io.patkaimark.chemaxontest;

import io.patkaimark.chemaxon.engines.ReadEngine;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


@Slf4j
public class ReadEngineTest {


    public static ReadEngine readEngine;



    @BeforeAll
    public static void init() {
        Assertions.assertDoesNotThrow(() -> {
            readEngine = ReadEngine.getInstance();
        });
    }


    @Test
    public void simpleValidInputTest() {
        log.info("Running simpleInputTest test for the input string of: AABBCC");
        final String input = "AABBCC";
        Assertions.assertTrue(readEngine.validateInput(input));
        log.info("Running simpleInputTest ran successfully!");
    }

    @Test
    public void simpleInvalidInputTest() {
        log.info("Running simpleInputTest test for the input string of: aabbcc");
        final String input = "aabbcc";
        Assertions.assertFalse(readEngine.validateInput(input));
        log.info("Running simpleInvalidInputTest ran successfully!");
    }

    @Test
    public void complexInputTest() {
        log.info("Running complexInputTest test for the input string of: A-B-C.S");
        final String input_complex_valid = "A-B-C.S";
        Assertions.assertTrue(readEngine.validateInput(input_complex_valid));
        log.info("Running complexInputTest ran successfully for string: A-B-C.S");

        final String input_complex_invalid = "A-B-c.S";
        log.info("Running complexInputTest test for the input string of: A-B-c.S");
        Assertions.assertFalse(readEngine.validateInput(input_complex_invalid));
        log.info("Running complexInputTest ran successfully for string: A-B-c.S");

        String input_complex_invalid_missing = "A-B-C.";
        log.info("Running complexInputTest test for the input string of: A-B-C.");
        Assertions.assertFalse(readEngine.validateInput(input_complex_invalid_missing));
        log.info("Running complexInputTest test for the input string of: A-B-c-");
        input_complex_invalid_missing = "A-B-C-";
        Assertions.assertFalse(readEngine.validateInput(input_complex_invalid_missing));
        log.info("Running complexInputTest ran successfully for string: [A-B-C. ; A-B-C-]");
    }

}
